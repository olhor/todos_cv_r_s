# frozen_string_literal: true

class TodosComponent < ViewComponentReflex::Component

  attr_reader :filter, :todo

  def initialize
    @filter = 'all'
  end

  def todos
    case filter
    when 'all'
      Todo.all
    when 'complete'
      Todo.complete
    when 'incomplete'
      Todo.incomplete
    else
      Todo.all
    end
  end

  def add(args = {})
    @todo = Todo.create(title: args[:title])
  end

  def toggle_filter
    @filter = element.value
  end

  def remove_completed
    Todo.complete.delete_all
  end
end
