Rails.application.routes.draw do
  resources :todos, only: [:index]
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get 'samples', to: 'application#samples'

  root to: 'application#todos'
end
