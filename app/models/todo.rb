class Todo < ApplicationRecord
  validates :title, presence: true, uniqueness: true

  scope :complete, -> { where.not(completed_at: nil) }
  scope :incomplete, -> { where(completed_at: nil) }

  def is_completed?
    completed_at.present?
  end
end
