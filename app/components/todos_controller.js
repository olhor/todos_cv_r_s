import ApplicationController from "../javascript/controllers/application_controller";
import './todos_component.scss'

export default class extends ApplicationController {
  static get targets() {
    return ['title']
  }

  add(ev) {
    console.log('adding new Todo')
    const target = ev.target
    if(ev.keyCode === 13) {
      this.stimulate('TodosComponent#add', { title: target.value })
    }
  }

  afterAdd(el, ev, error, reflexId) {
    el.todos.titleTarget.value = '' // check +ApplicationController -> initialize+ on howto reference particular stimulus controller
  }
}
