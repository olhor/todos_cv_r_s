# README

Sample application utilizing Stimulus JS, Action Cable Reflexes, Rails View Component. All in Material Design from Materialize framework.

### Features

* adding new todo item
* todo item validation: title presence and uniqueness
* title edition of incomplete todo item
* todo item list filtering: all, complete, incomplete
* marking todo item as complete/incomplete
* removing of all complete todo items
* information about how many incomplete todo items left
