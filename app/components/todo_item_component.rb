class TodoItemComponent < ViewComponentReflex::Component
  attr_reader :todo

  def initialize(todo:)
    @todo = todo
  end

  def collection_key
    @todo.id
  end

  def edit(args = {})
    @todo = Todo.find_by(id: args[:id])
    @todo.update(title: args[:title])
  end

  def toggle_completion
    @todo = Todo.find_by(id: element.dataset.id)
    @todo.update(completed_at: @todo.is_completed? ? nil : Time.current)
    refresh_all!
  end
end