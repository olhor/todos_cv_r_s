import ApplicationController from "../javascript/controllers/application_controller";

export default class extends ApplicationController {
  static get targets() {
    return ['title']
  }

  edit(ev) {
    const target = ev.target
    if(ev.keyCode === 13) {
      this.stimulate('TodoItemComponent#edit', { title: target.value, id: target.dataset.id })
    }
  }

  toggleEdition(ev) {
    const target = ev.target
    if(target.classList.contains('completed')) {
      return false
    }

    ev.type === 'focusin' ? target.readOnly = false : target.readOnly = true
  }
}
